from cms.app_base import CMSApp
from cms.apphook_pool import apphook_pool
from django.utils.translation import ugettext_lazy as _

class SwitchesApp(CMSApp):
    name = _("Switches App")
    urls = ["switches.urls"]

apphook_pool.register(SwitchesApp)
