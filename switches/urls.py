from django.conf.urls import url, patterns, include

urlpatterns = patterns('switches.views',
    (r'^$', 'index'),
    (r'^submit/$', 'postdata'),
    (r'^new/$', 'newrow'),
    (r'^remove/$', 'removerow'),
)
