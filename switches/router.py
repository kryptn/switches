class SwitchesRouter(object):



   def db_for_read(self, model, **hints):
      if model._meta.app_label == 'switches':
         return 'riot'
      return None

   def db_for_write(self, model, **hints):
      if model._meta.app_label == 'switches':
         return 'riot'
      return None

   def allow_relation(self, obj1, obj2, **hints):
      if obj1._meta.app_label == 'switches' or obj2._meta.app_label == 'switches':
         return True
      return None

   def allow_syncdb(self, db, model):
      if db == 'riot':
         return model._meta.app_label == 'switches'
      elif model._meta.app_label == 'switches':
         return False
      return None
