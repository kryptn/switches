from django.shortcuts import render, redirect
from switches.models import opendb, Switches
from django.template import RequestContext
from django.http import HttpResponse

def index(request):
   """
   display all switches in the database

   """
   with opendb() as db:
      cursor = db.cursor()
      cursor.execute("select * from switches")

   r = list()
   for (id, name, mac, rack, pod) in cursor:
      r.append({'id':id,'name':name,'mac':mac,'rack':rack,'pod':pod})

   return render(request, 'index.html',{'r':r,'authd':request.user.is_authenticated()},
                                        context_instance = RequestContext(request))
    
def postdata(request):
   """
   edit any edit class element inline through ajax calls
     columns are on the 'allowed' whitelist since it's set in
   the query statement itself

   """

   if request.is_ajax():
      allowed = ('name','mac','rack','pod')
      if request.POST['name'] in allowed:
         payload = (request.POST['value'],
                    int(request.POST['pk']))

         with opendb() as db:
            query = "UPDATE switches SET "+request.POST['name']+"=%s WHERE id=%s"
            cursor = db.cursor()
            cursor.execute(query, payload)
            db.commit()
        
      return(HttpResponse("x"))

def newrow(request):
   """ 
   add a row to the db
   to do: add mac validation, should be unique
  
   """
   
   payload = (request.POST['name'],
              request.POST['mac'],
              request.POST['rack'],
              request.POST['pod'],)
   query = """INSERT INTO switches (name, mac, rack, pod) VALUES(%s,%s,%s,%s)"""
   with opendb() as db:
      cursor = db.cursor()
      cursor.execute(query, payload)
      db.commit()
        
   return redirect('switches.views.index') 

def removerow(request):
   """ remove a row from the db """
   payload = (request.POST['id'],)
   query = """DELETE FROM switches WHERE id=%s"""
   with opendb() as db:
       cursor = db.cursor()
       cursor.execute(query, payload)
       db.commit()

   return redirect('switches.views.index')
