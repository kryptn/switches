from django.db import models
from django.conf import settings
import mysql.connector 
from contextlib import contextmanager

schema = """
CREATE TABLE switches (
id INT(12) NOT NULL auto_increment,
name VARCHAR(32),
mac VARCHAR(16),
rack VARCHAR(16),
pod VARCHAR(64),
primary KEY (id)); """

config = {'host':'localhost',
          'user':settings.DATABASES['riot']['USER'],
          'password':settings.DATABASES['riot']['PASSWORD'],
          'database':'riot',}


class Switches(models.Model):
   """
   Using the router in switches.router this allows
   built-in Django ORM to work with our external db.

   """
   name = models.CharField(max_length=32)
   mac = models.CharField(max_length=16)
   rack = models.CharField(max_length=16)
   pod = models.CharField(max_length=64)
   
   class Meta:
      db_table = "switches"


@contextmanager
def opendb():
   """ allows use of 'with opendb() as db' """
   try:
      db = mysql.connector.connect(**config)
      yield db
   finally:
      db.close()

def sync():
   """ loads table into fresh database """
   with opendb() as db:
      cursor = db.cursor()
      cursor.execute(schema)
      db.commit()


def load():
   """ load given data into db """
   with open('switches.txt') as f:
      data = f.read()
   with opendb() as db:
      cursor = db.cursor()
      query = "insert into switches (name,mac,rack,pod) values (%s,%s,%s,%s)"
      for x in data.split('\n')[:-1]:
         cursor.execute(query, x.split(','))
      db.commit()


      
