from switches.models import Switches
from rest_framework import viewsets
from rest_framework import filters
from switches_api.serializers import SwitchesSerializer

class SwitchesViewSet(viewsets.ModelViewSet):
   queryset = Switches.objects.all()
   serializer_class = SwitchesSerializer
   filter_backends = (filters.SearchFilter,)
   search_fields = ('name','rack')


