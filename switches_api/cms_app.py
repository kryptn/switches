from cms.app_base import CMSApp
from cms.apphook_pool import apphook_pool
from django.utils.translation import ugettext_lazy as _

class SwitchesApi(CMSApp):
    name = _("Switches Api")
    urls = ["switches_api.urls"]

apphook_pool.register(SwitchesApi)
