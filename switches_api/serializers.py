from switches.models import Switches
from rest_framework import serializers

class SwitchesSerializer(serializers.HyperlinkedModelSerializer):

	pod = serializers.CharField(required=False)

   class Meta:
      model = Switches
      