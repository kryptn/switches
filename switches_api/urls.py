from django.conf.urls import patterns, url, include
from rest_framework import routers
from switches_api import views

router = routers.DefaultRouter()
router.register(r'switches', views.SwitchesViewSet)

urlpatterns = patterns('',
   url('^', include(router.urls)),
   url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
   url(r'^docs/', include('rest_framework_swagger.urls')),
)
