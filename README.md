#Switches
#### Used to track instances in a physical location to assist sysadmins and database techs
____

##Created with the following specifications:
* Use Django and Django-CMS
* Don't use built-in Django ORM Database
* Develop a web app to modify a database
* Use django_rest for a RESTful interface

## Demo

curl can be used to query from REST through <http://website.com/en/api/switches>

Example:

    curl -X POST -H "Content-Type: application/json" -d '{"name": "name", "mac": "aabbccddeeff", "rack": "F116", "pod": "1"}' "http://website.com/en/api/switches/" -u user:pass
 
## Setup

Add to settings.py:

    INSTALLED_APPS = ['
    'cms',
    ...
    'switches',
    'switches_api',
    'django_rest',
    'rest_framework',
    'rest_framework_swagger',
    ...
    ']

    DATABASE_ROUTERS = ['switches.router.SwitchesRouter']

    REST_FRAMEWORK = {
        'DEFAULT_PERMISSION_CLASSES': ('rest_framework.permissions.IsAdminUser',),
        'DEFAULT_FILTER_BACKENDS': ('rest_framework.filters.DjangoFilterBackend',
                                    'rest_framework.filters.SearchFilter',),
    }